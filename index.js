var countryCount = {}, users = {}
var countries = {"AF" : "AFG", "AX" : "ALA", "AL" : "ALB", "DZ" : "DZA", "AS" : "ASM", "AD" : "AND", "AO" : "AGO", "AI" : "AIA", "AQ" : "ATA", "AG" : "ATG", "AR" : "ARG", "AM" : "ARM", "AW" : "ABW", "AU" : "AUS", "AT" : "AUT", "AZ" : "AZE", "BS" : "BHS", "BH" : "BHR", "BD" : "BGD", "BB" : "BRB", "BY" : "BLR", "BE" : "BEL", "BZ" : "BLZ", "BJ" : "BEN", "BM" : "BMU", "BT" : "BTN", "BO" : "BOL", "BQ" : "BES", "BA" : "BIH", "BW" : "BWA", "BV" : "BVT", "BR" : "BRA", "IO" : "IOT", "BN" : "BRN", "BG" : "BGR", "BF" : "BFA", "BI" : "BDI", "KH" : "KHM", "CM" : "CMR", "CA" : "CAN", "CV" : "CPV", "KY" : "CYM", "CF" : "CAF", "TD" : "TCD", "CL" : "CHL", "CN" : "CHN", "CX" : "CXR", "CC" : "CCK", "CO" : "COL", "KM" : "COM", "CG" : "COG", "CD" : "COD", "CK" : "COK", "CR" : "CRI", "CI" : "CIV", "HR" : "HRV", "CU" : "CUB", "CW" : "CUW", "CY" : "CYP", "CZ" : "CZE", "DK" : "DNK", "DJ" : "DJI", "DM" : "DMA", "DO" : "DOM", "EC" : "ECU", "EG" : "EGY", "SV" : "SLV", "GQ" : "GNQ", "ER" : "ERI", "EE" : "EST", "ET" : "ETH", "FK" : "FLK", "FO" : "FRO", "FJ" : "FJI", "FI" : "FIN", "FR" : "FRA", "GF" : "GUF", "PF" : "PYF", "TF" : "ATF", "GA" : "GAB", "GM" : "GMB", "GE" : "GEO", "DE" : "DEU", "GH" : "GHA", "GI" : "GIB", "GR" : "GRC", "GL" : "GRL", "GD" : "GRD", "GP" : "GLP", "GU" : "GUM", "GT" : "GTM", "GG" : "GGY", "GN" : "GIN", "GW" : "GNB", "GY" : "GUY", "HT" : "HTI", "HM" : "HMD", "VA" : "VAT", "HN" : "HND", "HK" : "HKG", "HU" : "HUN", "IS" : "ISL", "IN" : "IND", "ID" : "IDN", "IR" : "IRN", "IQ" : "IRQ", "IE" : "IRL", "IM" : "IMN", "IL" : "ISR", "IT" : "ITA", "JM" : "JAM", "JP" : "JPN", "JE" : "JEY", "JO" : "JOR", "KZ" : "KAZ", "KE" : "KEN", "KI" : "KIR", "KP" : "PRK", "KR" : "KOR", "KW" : "KWT", "KG" : "KGZ", "LA" : "LAO", "LV" : "LVA", "LB" : "LBN", "LS" : "LSO", "LR" : "LBR", "LY" : "LBY", "LI" : "LIE", "LT" : "LTU", "LU" : "LUX", "MO" : "MAC", "MK" : "MKD", "MG" : "MDG", "MW" : "MWI", "MY" : "MYS", "MV" : "MDV", "ML" : "MLI", "MT" : "MLT", "MH" : "MHL", "MQ" : "MTQ", "MR" : "MRT", "MU" : "MUS", "YT" : "MYT", "MX" : "MEX", "FM" : "FSM", "MD" : "MDA", "MC" : "MCO", "MN" : "MNG", "ME" : "MNE", "MS" : "MSR", "MA" : "MAR", "MZ" : "MOZ", "MM" : "MMR", "NA" : "NAM", "NR" : "NRU", "NP" : "NPL", "NL" : "NLD", "NC" : "NCL", "NZ" : "NZL", "NI" : "NIC", "NE" : "NER", "NG" : "NGA", "NU" : "NIU", "NF" : "NFK", "MP" : "MNP", "NO" : "NOR", "OM" : "OMN", "PK" : "PAK", "PW" : "PLW", "PS" : "PSE", "PA" : "PAN", "PG" : "PNG", "PY" : "PRY", "PE" : "PER", "PH" : "PHL", "PN" : "PCN", "PL" : "POL", "PT" : "PRT", "PR" : "PRI", "QA" : "QAT", "RE" : "REU", "RO" : "ROU", "RU" : "RUS", "RW" : "RWA", "BL" : "BLM", "SH" : "SHN", "KN" : "KNA", "LC" : "LCA", "MF" : "MAF", "PM" : "SPM", "VC" : "VCT", "WS" : "WSM", "SM" : "SMR", "ST" : "STP", "SA" : "SAU", "SN" : "SEN", "RS" : "SRB", "SC" : "SYC", "SL" : "SLE", "SG" : "SGP", "SX" : "SXM", "SK" : "SVK", "SI" : "SVN", "SB" : "SLB", "SO" : "SOM", "ZA" : "ZAF", "GS" : "SGS", "SS" : "SSD", "ES" : "ESP", "LK" : "LKA", "SD" : "SDN", "SR" : "SUR", "SJ" : "SJM", "SZ" : "SWZ", "SE" : "SWE", "CH" : "CHE", "SY" : "SYR", "TW" : "TWN", "TJ" : "TJK", "TZ" : "TZA", "TH" : "THA", "TL" : "TLS", "TG" : "TGO", "TK" : "TKL", "TO" : "TON", "TT" : "TTO", "TN" : "TUN", "TR" : "TUR", "TM" : "TKM", "TC" : "TCA", "TV" : "TUV", "UG" : "UGA", "UA" : "UKR", "AE" : "ARE", "GB" : "GBR", "US" : "USA", "UM" : "UMI", "UY" : "URY", "UZ" : "UZB", "VU" : "VUT", "VE" : "VEN", "VN" : "VNM", "VG" : "VGB", "VI" : "VIR", "WF" : "WLF", "EH" : "ESH", "YE" : "YEM", "ZM" : "ZMB", "ZW" : "ZWE"}
// var latlng = [];
$.xhrPool = [];
$.ajaxSetup({
  beforeSend: function(jqXHR, settings){
    $.xhrPool.push(jqXHR);
  },
  complete: function(jqXHR){
    var index = $.xhrPool.indexOf(jqXHR);
    if (index > -1) 
      $.xhrPool.splice(index, 1);

    if($.xhrPool.length == 0)
      getLocations();
  }
})
for(var i=0;i<3;i++){
  $.ajax({
    crossDomain: true,
    type:"GET",
    async:true,
    data: {
      apikey: '09C43A9B270A470B8EB8F2946A9369F3',
      q: 'soof',
      mintime: '1142812800',
      maxtime: '1416096000',
      sort_by: '-date',
      allow_lang: 'en',
      tweet_types: 'tweet',
      limit: '200',
      offset: i*200
    },
    url: 'http://api.topsy.com/v2/content/tweets.js',
    dataType: "jsonp",
    success: function(r){
      fillUsers(r.response.results.list);
    }
  });
}

function fillUsers(tweets){
  for(var i=0;i<tweets.length;i++){
    var userName = tweets[i].tweet.user.screen_name;
    if(tweets[i].tweet.user.location!=null && tweets[i].tweet.user.location.replace(/[^a-zA-Z0-9., ]/g,"").trim().length!=0){
      if(users[userName]===undefined)
        users[userName] = {tweets: 1, location: tweets[i].tweet.user.location.replace(/[^a-zA-Z0-9., ]/g,"")}
      else
        users[userName].tweets++;
    }
  }
}

function getLocations(){
  $.xhrPool = [];
  $.ajaxSetup({
    beforeSend: function(jqXHR){
      $.xhrPool.push(jqXHR);
    },
    complete: function(jqXHR){
      var index = $.xhrPool.indexOf(jqXHR);
      if (index > -1) 
        $.xhrPool.splice(index, 1);

      if($.xhrPool.length == 0){
        for(var c in countryCount)
          $('path.datamaps-subunit.'+c).css('fill', $($('.datamaps-legend dd')[Math.floor(Math.log10(countryCount[c]))]).css('background-color'));
        // map.bubbles(latlng);
      }
    }
  })
  var count = 0, u="";
  for(var key in users){
    count++;
    u+="&location="+users[key].location;
    if(count%100==0){
      count=0;
      makeLocationRequest(u);
      u="";
    }
  }
  makeLocationRequest(u);
}

function makeLocationRequest(locations){
  $.ajax({
    crossDomain: true,
    type: "GET",
    async: true,
    data: {      
      thumbMaps: false,
      maxResults: 1
    },
    url: "http://www.mapquestapi.com/geocoding/v1/batch?key=Fmjtd%7Cluurn9u12h%2C7g%3Do5-9wzggw"+locations,
    dataType: 'jsonp',
    success: function(r){
      for(var i=0;i<r.results.length;i++){
        var country= countries[r.results[i].locations[0].adminArea1];
        // latlng.push({latitude: r.results[i].locations[0].displayLatLng.lat, longitude: r.results[i].locations[0].displayLatLng.lng, radius: 3, fillKey: 'point'})
        if(countryCount[country]===undefined)
          countryCount[country] = 1;
        else
          countryCount[country]++;
      }
    }
  })
}